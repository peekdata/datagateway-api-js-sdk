This is an SDK for [Peekdata API](https://peekdata.io/developers/index.html#/api/).

## Supported APIs

- Configuration API - for managing engine configuration
- Data model API - for fetching available Scopes, Data Models, Cubes,  
  Dimensions, Metrics. It also lets you to reload Data Model  
  configurations.
- Data API - for fetching data in various formats or generated SQL  
  queries
- Validation API - for validating data requests and getting valid  
  combinations of dimensions and metrics
- Healthcheck API - for monitoring system status
- Report API - for managing Reports

## Installation

Install SDK using:

`npm install --save peekdata-datagateway-api-sdk`

or

`yarn add peekdata-datagateway-api-sdk`

## Initialization and Confifuration

More information about available SDK methods and how to initialize the SDK can be found in [docs](https://peekdata.io/developers/index.html#/js-sdk/?id=peekdata-reactjs-sdk-for-reporing-and-data-api).

## More examples

- [node-js-example.js](https://gitlab.com/peekdata/datagateway-api-js-sdk/-/blob/master/node-js-example.js) a sample implementation of all main `Peekdata Data API` calls using this SDK.

- [Chart.js example](https://reportbuilder.peekdata.io/examples/chartsjs) an example of showing reports `Charts.js` using this SDK.
