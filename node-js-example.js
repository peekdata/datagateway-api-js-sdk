global.fetch = require("node-fetch");

const {
  PeekdataApi,
  ReportFilterOperationType,
  ReportSortDirectionType,
  DataSet,
  ReportApi,
} = require("./lib");

// #region -------------- Api -------------------------------------------------------------------

const api = new PeekdataApi({
  baseUrl: `https://demo.peekdata.io:8443/datagateway/rest/v1`,
  timeout: 60000,
});

// #endregion

// #region -------------- Report request -------------------------------------------------------------------

const scopeName = "Mortgage-Lending";
const dataModelName = "Servicing-PostgreSQL";

const dimensions = ["cityname", "currency", "countryname"];

const metrics = ["loanamount", "propertyprice"];

const reportRequest = {
  scopeName,
  dataModelName,
  requestID: 123456,
  consumerInfo: "Example JS Client",
  dimensions,
  metrics,
  filters: {
    dateRanges: [
      {
        from: "2015-01-01",
        to: "2017-12-31",
        key: "closingdate",
      },
    ],
    singleKeys: [
      {
        key: "countryname",
        operation: ReportFilterOperationType.EQUALS,
        values: ["Poland"],
      },
      {
        key: "propertycityid",
        operation: ReportFilterOperationType.EQUALS,
        values: ["30", "35", "26"],
      },
    ],
  },
  sortings: {
    dimensions: [
      {
        key: "currency",
        direction: ReportSortDirectionType.ASC,
      },
      {
        key: "countryname",
      },
      {
        key: "cityname",
        direction: ReportSortDirectionType.DESC,
      },
    ],
  },
  options: {
    arguments: {},
    rows: {
      startWithRow: "0",
      limitRowsTo: "2",
    },
  },
};

const compatibilityRequest = {
  scopeName,
  dataModelName,
  requestID: 123456,
  consumerInfo: "Example JS Client",
  dimensions,
  metrics,
};

// Bad report request: no metric selected
const badReportRequest = {
  scopeName,
  dataModelName,
  requestID: 123456,
  consumerInfo: "Example JS Client",
  dimensions,
  options: {
    rows: {
      startWithRow: "0",
      limitRowsTo: "1",
    },
  },
};

// #endregion

// #region -------------- Helper methods -------------------------------------------------------------------

function getJsonString(json) {
  return JSON.stringify(json, null, 2);
}

// #endregion

// #region -------------- Api usage examples -------------------------------------------------------------------

async function runCompatibilityApiExample() {
  console.log("Start: runCompatibilityApiExample()");
  const validationResponse = await api.datamodel.validate(compatibilityRequest);
  const compatibleNodes = await api.datamodel.getCompatibleNodes(
    compatibilityRequest
  );

  console.log(
    getJsonString({
      validationResponse,
      compatibleNodes,
    })
  );
  console.log("End: runCompatibilityApiExample()");
}

async function runDataApiExample() {
  console.log("Start: runDataApiExample()");

  const query = await api.data.getQuery(reportRequest);
  const data = await api.data.getData(reportRequest);
  const optimizedData = await api.data.getDataOptimized(reportRequest);
  const csv = await api.data.getCSV(reportRequest);
  const dataSet = new DataSet(optimizedData);

  console.log(
    getJsonString({
      query,
      data,
      optimizedData,
      csv,
      dimensionsValues: dataSet.getDimensionsValues(),
      metricsValues: dataSet.getMetricsValues(),
      values: dataSet.getValues(),
    })
  );
  console.log("End: runDataApiExample()");
}

async function runDataApiExampleWithBadRequest() {
  console.log("Start: runDataApiExampleWithBadRequest()");
  try {
    await api.data.getDataOptimized(badReportRequest);
  } catch (error) {
    console.log(error.toString());
  }
  console.log("End: runDataApiExampleWithBadRequest()");
}

async function runDataModelExample() {
  console.log("Start: runDataModelExample()");
  const scopeNames = await api.datamodel.getScopeNames();
  console.log(getJsonString(scopeNames));
  const dataModelNames = await api.datamodel.getDataModelNames(scopeName);
  console.log(getJsonString(dataModelNames));
  const cubeNames = await api.datamodel.getCubeNames(scopeName, dataModelName);
  console.log(getJsonString(cubeNames));
  const dimensions = await api.datamodel.getDimensions(
    scopeName,
    dataModelName
  );
  console.log(getJsonString(dimensions));
  const metrics = await api.datamodel.getMetrics(scopeName, dataModelName);
  console.log(getJsonString(metrics));

  console.log(
    getJsonString({
      scopeNames,
      dataModelNames,
      cubeNames,
      dimensions,
      metrics,
    })
  );
  console.log("End: runDataModelExample()");
}

async function runHealthApiExample() {
  console.log("Start: runHealthApiExample()");
  const health = await api.health.getHealth();
  const healthDetails = await api.health.getHealthDetails();
  const connectionsHealth = await api.health.getConnectionsHealth();
  const dataModelsHealth = await api.health.getDataModelsHealth();

  console.log(
    getJsonString({
      health,
      healthDetails,
      connectionsHealth,
      dataModelsHealth,
    })
  );
  console.log("End: runHealthApiExample()");
}

const reportApi = new ReportApi({
  baseUrl: "http://localhost:8080/v1/",
});

async function runReportApiExample() {
  console.log("Start: runReportExample()");

  console.log("Get report list");
  const reportsInitial = await reportApi.getReports();
  console.log(getJsonString(reportsInitial));

  console.log("Create report");
  const addedReport = await reportApi.saveReport({
    name: "Test report",
    requestParams: reportRequest,
  });
  console.log(getJsonString(addedReport));

  console.log("Update report");
  const updatedReport = await reportApi.updateReport(addedReport.id, {
    ...addedReport,
    requestParams: {
      ...addedReport.requestParams,
      metrics: ["loanamount"],
    },
    name: "Test report updated",
  });
  console.log(getJsonString(updatedReport));

  const allReportsAfterUpdate = await reportApi.getReports();
  console.log(getJsonString(allReportsAfterUpdate));

  // console.log("Delete report");
  // await reportApi.deleteReport(updatedReport.id);

  const allReportsAfterDelete = await reportApi.getReports();
  console.log(getJsonString(allReportsAfterDelete));

  console.log("End: runReportExample()");
}

// #endregion

// #region -------------- Execute api examples -------------------------------------------------------------------

async function runAllExamples() {
  try {
    await runCompatibilityApiExample();
    await runDataApiExample();
    await runDataApiExampleWithBadRequest();
    await runDataModelExample();
    await runHealthApiExample();

    process.exit();
  } catch (error) {
    console.error(error.message);
  }
}

async function runReportExample() {
  try {
    await runReportApiExample();

    process.exit();
  } catch (error) {
    console.error(error.message);
  }
}

runAllExamples();
runReportExample();

// #endregion
