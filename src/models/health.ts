export interface IScopeHealth {
  name: string;
  overallStatus: string;
  dataModels: IDataModelHealth[];
}

export interface IDataModelHealth {
  name: string;
  dataModelStatus: string;
  databaseStatus: string;
}

export interface IOverallHealth {
  scopes: IScopeHealth[];
  overallStatus: string;
}

export interface IConnectionsHealth {
  [connection: string]: string;
}

export interface IDataModelsHealth {
  [dataModelName: string]: string;
}
