export interface IApiError {
  requestID: string;
  message: string;
  errorCode: ApiErrorCode;
}

export enum ApiErrorCode {
    HealthCheckFailed = 50503,
    FeatureIsNotSupportedOrImplemented = 59010,
    ServiceIsNotEnabled = 59020,
    SecurityRoleIsMandatory = 59030,
    DimensionMetricCombinationIsNotValid = 51010,
    CannotGetParseArguments = 51020,
    TooManyWaysGettingData = 51030,
    FailedToParseName = 51040,
    DuplicatedIncorrectObjectName = 51050,
    FailedParseExpression = 51060,
    InvalidNodeOrLink = 51070,
    DimensionsMetricsAreNotLinked = 51080,
    DimensionsMetricsIncorrectRelations = 51090,   
    BadDataModelName = 51220,
    BadScopeName = 51230,
    FailedGettingDimensionNodes = 51240,
    FailedGettingMetricNodes = 51250,
    FailedGettingCubesTables = 51260,
    FailedReadConfiguration = 51300,
    FailedReadDataModel = 52010,
    FailedCloseReader = 53010,
    FailedToCast = 53020,
    UnsupportedEncoding = 53030,
    ScopeIsMandatory = 55010,
    MetricIsMandatory = 55020,
    DataModelNameIsMandatory = 55030,
    FailedExecuteQuery = 56010,
    DimensionsMetricsNotCompatible = 57010
}
