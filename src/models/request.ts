export interface IRequestHeaders {
  [key: string]: string;
}

export interface IRequestOptions {
  baseUrl?: string;

  /**
   * Request timeout in milliseconds.
   * Default: 60000 ms (1 minute).
   */
  timeout?: number;

  headers?: IRequestHeaders;
}

export interface IRequestMethodOptions<Body = void> {
  path: string;
  params?: IRequestParameter[];
  body?: Body;
  requestOptions: IRequestOptions;
}

export interface IRequestParameter {
  key: string;
  value: string;
}
