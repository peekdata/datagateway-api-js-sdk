import { ApiErrorCode } from './error';

export interface INode {
  name: string;
  title: string;
  description: string;
  groups: string[];
  dataType: string;
}

export interface ICompatibilityResponse {
  requestID: string;
  message: string;
  errorCode: ApiErrorCode;
  dimensions: INode[];
  metrics: INode[];
}

export interface IValidationResponse {
  requestID: string;
  message: string;
  errorCode: ApiErrorCode;
  isValid: boolean;
}
