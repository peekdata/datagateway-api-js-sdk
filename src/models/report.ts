// #region -------------- Filter -------------------------------------------------------------------

export enum ReportFilterOperationType {
  EQUALS = "EQUALS",
  NOT_EQUALS = "NOT_EQUALS",
  STARTS_WITH = "STARTS_WITH",
  NOT_STARTS_WITH = "NOT_STARTS_WITH",
  ENDS_WITH = "ENDS_WITH",
  NOT_ENDS_WITH = "NOT_ENDS_WITH",
  CONTAINS = "CONTAINS",
  NOT_CONTAINS = "NOT_CONTAINS",
  ALL_IS_LESS = "ALL_IS_LESS",
  ALL_IS_MORE = "ALL_IS_MORE",
  AT_LEAST_ONE_IS_LESS = "AT_LEAST_ONE_IS_LESS",
  AT_LEAST_ONE_IS_MORE = "AT_LEAST_ONE_IS_MORE",
  IS_NULL = "IS_NULL",
  IS_NOT_NULL = "IS_NOT_NULL",
}

export interface IReportFilterDateRange {
  key: string;
  from: string;
  to: string;
}

export interface IReportFilterSingleKey {
  key: string;
  operation: ReportFilterOperationType;
  values: string[];
}

export interface IReportFilterSingleValue {
  keys: string[];
  operation: ReportFilterOperationType;
  value: string;
}

export interface IReportFilters {
  dateRanges?: IReportFilterDateRange[];
  singleKeys?: IReportFilterSingleKey[];
  singleValues?: IReportFilterSingleValue[];
}

// #endregion

// #region -------------- Sorting -------------------------------------------------------------------

export enum ReportSortDirectionType {
  ASC = "ASC",
  DESC = "DESC",
}

export interface IReportRequestSortKey {
  key: string;
  direction?: ReportSortDirectionType;
  priority?: number;
}

export interface IReportRequestSortings {
  dimensions?: IReportRequestSortKey[];
  metrics?: IReportRequestSortKey[];
}

// #endregion

// #region -------------- Options -------------------------------------------------------------------

export interface IReportRequestRowsOptions {
  /**
   * In order to limit row set use limitRowsTo
   */
  limitRowsTo?: string;

  /**
   * Using "startWithRow": N will allow skipping first N rows. This could be useful when data paging is needed.
   */
  startWithRow?: string;
}

export interface IReportRequestArguments {
  [key: string]: string | number | boolean;
}

export interface IReportRequestOptions {
  rows?: IReportRequestRowsOptions;

  /**
   * There is a possibility to parametrize each expression within Data Model configuration with arguments
   */
  arguments?: IReportRequestArguments;
}

// #endregion

// #region -------------- Compatibility request -------------------------------------------------------------------

export interface ICompatibilityCheckRequest {
  /**
   * Request ID is needed for analysis of whole chain of execution as it is passed starting from request till execution within engine.
   */
  requestID?: string;

  /**
   * Used for information only when debugging so not used within engine.
   */
  consumerInfo?: string;

  /**
   * The name of Business scope or business domain.
   */
  scopeName: string;

  /**
   * Used when your data request is tend to get data from specific data source.
   * If this parameter is not set - engine will request each data model for the valid combination and choose the one depending on priorities set.
   */
  dataModelName?: string;

  securityRole?: string;
  dimensions?: string[];
  metrics: string[];
}

// #endregion

// #region -------------- Request -------------------------------------------------------------------

export interface IReportRequest extends ICompatibilityCheckRequest {
  /**
   * Used to form criterias for data selections.
   */
  filters?: IReportFilters;

  /**
   * Used to define sorting order for dimensions and metrics.
   */
  sortings?: IReportRequestSortings;
  options?: IReportRequestOptions;
}

// #endregion

// #region -------------- Response -------------------------------------------------------------------

export enum ReportColumnType {
  dimension = "dimension",
  metric = "metric",
}

export interface IReportColumnHeader {
  title: string;
  name: string;
  dataType: string;
  format: string;
  alias: string;
  columnType: ReportColumnType;
}

export type IDimensionValue = string | null | undefined;
export type IMetricValue = number | null | undefined;
export type IReportRowValue = IDimensionValue | IMetricValue;

export interface IReportNotOptimizedRow {
  [key: string]: IReportRowValue;
}

export type IReportOptimizedDataRow = IReportRowValue[];

export interface IReportResponse<Row> {
  requestID: string;
  columnHeaders: IReportColumnHeader[];
  rows: Row[];
  totalRows: number;
}

export interface INotOptimizedReportResponse
  extends IReportResponse<IReportNotOptimizedRow> {}

export interface IOptimizedReportResponse
  extends IReportResponse<IReportOptimizedDataRow> {}

// #endregion

// #region -------------- Data set -------------------------------------------------------------------

export interface IMetricValues {
  metric: IReportColumnHeader;
  values: IMetricDimensionsValue[];
}

export interface IMetricDimensionsValue {
  dimensions: IDimensionValue[];
  value: IMetricValue;
}

export interface ISingleValue {
  metric: IReportColumnHeader;
  dimensions: IDimensionValue[];
  value: IMetricValue;
}

// #endregion

export interface IReportListItem {
  requestParams: IReportRequest;
}

export interface IReportListItemRequest extends IReportListItem {
  id?: string;
  name?: string;
  userId?: string;
}

export interface IReportListItemResponse extends IReportListItem {
  id: string;
  name: string;
  userId?: string;
  created: string;
  modified?: string;
}
