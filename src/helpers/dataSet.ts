import { IDimensionValue, IMetricDimensionsValue, IMetricValue, IMetricValues, IReportColumnHeader, IReportNotOptimizedRow, IReportOptimizedDataRow, IReportResponse, IReportRowValue, ISingleValue, ReportColumnType } from '../models/report';

export class DataSet {
  private response: IReportResponse<IReportNotOptimizedRow | IReportOptimizedDataRow>;
  private dimensionIndexes: number[];
  private metricIndexes: number[];

  public constructor(response: IReportResponse<IReportNotOptimizedRow | IReportOptimizedDataRow>) {
    this.response = response;

    const columnHeaders = this.response && this.response.columnHeaders;
    this.dimensionIndexes = this.getColumnsIndexes(columnHeaders, ReportColumnType.dimension);
    this.metricIndexes = this.getColumnsIndexes(columnHeaders, ReportColumnType.metric);
  }

  // #region -------------- Public methods -------------------------------------------------------------------

  /**
   * Returns raw data response
   */
  public getRawResponse() {
    return this.response;
  }

  /**
   * Returns dimensions values
   */
  public getDimensionsValues(): IDimensionValue[][] {
    if (!this.response || !this.dimensionIndexes || this.dimensionIndexes.length < 1) {
      return null;
    }

    const { rows } = this.response;
    const dimensions = [];

    if (!rows || rows.length < 1) {
      return null;
    }

    for (const row of rows) {
      const dimensionValues: IDimensionValue[] = [];
      const optimizedRow = this.getOptimizedDataRow(row);

      for (const index of this.dimensionIndexes) {
        dimensionValues.push(optimizedRow[index] as IDimensionValue);
      }

      dimensions.push(dimensionValues);
    }

    return dimensions;
  }

  /**
   * Returns all values mapped by metrics
   */
  public getMetricsValues(): IMetricValues[] {
    if (!this.response || !this.metricIndexes || this.metricIndexes.length < 1) {
      return null;
    }

    const { columnHeaders, rows } = this.response;
    const dataSet: IMetricValues[] = [];

    if (!rows || rows.length < 1) {
      return null;
    }

    for (const index of this.metricIndexes) {
      const dataset: IMetricDimensionsValue[] = [];

      for (const row of rows) {
        const optimizedRow = this.getOptimizedDataRow(row);

        dataset.push({
          dimensions: this.getRowDimensionsValues(optimizedRow),
          value: optimizedRow[index] as IMetricValue,
        });
      }

      dataSet.push({
        metric: columnHeaders[index],
        values: dataset,
      });
    }

    return dataSet;
  }

  /**
   * Returns all values
   */
  public getValues(): ISingleValue[] {
    if (!this.response || !this.metricIndexes || this.metricIndexes.length < 1) {
      return null;
    }

    const { columnHeaders, rows } = this.response;
    const values: ISingleValue[] = [];

    if (!rows || rows.length < 1) {
      return null;
    }

    for (const index of this.metricIndexes) {
      for (const row of rows) {
        const optimizedRow = this.getOptimizedDataRow(row);

        values.push({
          metric: columnHeaders[index],
          dimensions: this.getRowDimensionsValues(optimizedRow),
          value: optimizedRow[index] as IMetricValue,
        });
      }
    }

    return values;
  }

  // #endregion

  // #region -------------- Private methods -------------------------------------------------------------------

  private getColumnsIndexes(columnHeaders: IReportColumnHeader[], columnType: ReportColumnType): number[] {
    const indexes: number[] = [];

    if (!columnHeaders || columnHeaders.length < 1 || !columnType) {
      return indexes;
    }

    columnHeaders.forEach((columnHeader, index) => {
      const headerColumnType = columnHeader && columnHeader.columnType;

      if (headerColumnType === columnType) {
        indexes.push(index);
      }
    });

    return indexes;
  }

  private getRowDimensionsValues(row: IReportOptimizedDataRow): IDimensionValue[] {
    return this.dimensionIndexes && this.dimensionIndexes.map(index => {
      return row[index] as IDimensionValue;
    });
  }

  private getOptimizedDataRow(row: IReportNotOptimizedRow | IReportRowValue[]): IReportRowValue[] {
    if (!row) {
      return null;
    }

    if (this.isOptimizedDataRow(row)) {
      return row;
    }

    const optimizedRow: IReportRowValue[] = [];
    const columnHeaders = this.response.columnHeaders;

    if (!columnHeaders) {
      return null;
    }

    columnHeaders.forEach(header => {
      optimizedRow.push(row[header.name]);
    });

    return optimizedRow;
  }

  private isOptimizedDataRow(row: IReportNotOptimizedRow | IReportRowValue[]): row is IReportRowValue[] {
    return Array.isArray(row);
  }

  // #endregion
}
