import { ICompatibilityResponse, INode, IValidationResponse } from '../../models/datamodel';
import { ICompatibilityCheckRequest } from '../../models/report';
import { IRequestOptions, IRequestParameter } from '../../models/request';
import { BaseApi } from '../base';

export class DataModelApi extends BaseApi {

  /**
   * Gets list of scope names
   */
  public getScopeNames = (requestOptions?: IRequestOptions) => {
    return this.get<string[]>({
      path: '/datamodel/getscopenames',
      requestOptions,
    });
  }

  /**
   * Gets list of data model names
   */
  public getDataModelNames = (scopeName: string, requestOptions?: IRequestOptions) => {
    return this.get<string[]>({
      path: '/datamodel/getdatamodelnames',
      params: [
        { key: 'scopeName', value: scopeName },
      ],
      requestOptions,
    });
  }

  /**
   * Gets list of cube names
   */
  public getCubeNames = (scopeName: string, dataModelName: string, requestOptions?: IRequestOptions) => {
    return this.get<string[]>({
      path: '/datamodel/getcubes',
      params: [
        { key: 'scopeName', value: scopeName },
        { key: 'dataModelName', value: dataModelName },
      ],
      requestOptions,
    });
  }

  /**
   * Gets list of data model's dimensions
   * @param securityRole used to filter dimensions
   */
  public getDimensions = (scopeName: string, dataModelName: string, securityRole?: string, requestOptions?: IRequestOptions) => {
    const params: IRequestParameter[] = [
      { key: 'scopeName', value: scopeName },
      { key: 'dataModelName', value: dataModelName },
    ];

    if (securityRole) {
      params.push({ key: 'securityRole', value: securityRole });
    }

    return this.get<INode[]>({
      path: '/datamodel/getdimensions',
      params,
      requestOptions,
    });
  }

  /**
   * Gets list of data model's metrics
   * @param securityRole used to filter metrics
   */
  public getMetrics = (scopeName: string, dataModelName: string, securityRole?: string, requestOptions?: IRequestOptions) => {
    const params: IRequestParameter[] = [
      { key: 'scopeName', value: scopeName },
      { key: 'dataModelName', value: dataModelName },
    ];

    if (securityRole) {
      params.push({ key: 'securityRole', value: securityRole });
    }

    return this.get<INode[]>({
      path: '/datamodel/getmetrics',
      params,
      requestOptions,
    });
  }

  /**
   * As Data API should receive valid combinations of dimensions and metrics, request should be validated first.
   * If combination is not valid, field "isValid" will return false with detailed description contained in "reason".
   */
  public validate = (request: ICompatibilityCheckRequest, requestOptions?: IRequestOptions) => {
    return this.post<IValidationResponse, ICompatibilityCheckRequest>({
      path: '/datamodel/validate',
      body: request,
      requestOptions,
    });
  }

  /**
   * Gets valid dimensions and metrics
   */
  public getCompatibleNodes = (request: ICompatibilityCheckRequest, requestOptions?: IRequestOptions) => {
    return this.post<ICompatibilityResponse, ICompatibilityCheckRequest>({
      path: '/datamodel/getcompatible',
      body: request,
      requestOptions,
    });
  }
}
