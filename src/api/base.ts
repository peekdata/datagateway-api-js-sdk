import { defaultTimeout } from "../constants";
import { IApiError } from "../models/error";
import {
  IRequestHeaders,
  IRequestMethodOptions,
  IRequestOptions,
  IRequestParameter,
} from "../models/request";
import { PeekdataError } from "./PeekdataError";

// #region -------------- Class -------------------------------------------------------------------

export class BaseApi {
  protected baseUrl: string;
  protected timeout: number;
  protected headers: IRequestHeaders;

  constructor(options: IRequestOptions) {
    const { baseUrl, timeout, headers } = options;

    this.timeout = timeout ? timeout : defaultTimeout;
    this.baseUrl = baseUrl;
    this.headers = headers;
  }

  // #region -------------- Setters -------------------------------------------------------------------

  public setTimeout = (timeout: number) => {
    this.timeout = timeout;
  };

  public setBaseUrl = (baseUrl: string) => {
    this.baseUrl = baseUrl;
  };

  public setHeaders = (headers: IRequestHeaders) => {
    this.headers = headers;
  };

  // #endregion

  // #region -------------- Request -------------------------------------------------------------------

  protected post = <Return, Body = void>(
    options: IRequestMethodOptions<Body>
  ) => {
    return this.call<Return, Body>(options, "POST");
  };

  protected get = <Return>(options: IRequestMethodOptions) => {
    return this.call<Return>(options, "GET");
  };

  protected put = <Return, Body = void>(
    options: IRequestMethodOptions<Body>
  ) => {
    return this.call<Return, Body>(options, "PUT");
  };

  protected delete = <Return>(options: IRequestMethodOptions) => {
    return this.call<Return>(options, "DELETE");
  };

  private call = <Return, Body = void>(
    options: IRequestMethodOptions<Body>,
    action: "POST" | "GET" | "PUT" | "DELETE"
  ) => {
    const { path, params, body, requestOptions } = options;
    const timeout = (requestOptions && requestOptions.timeout) || this.timeout;
    const baseUrl = (requestOptions && requestOptions.baseUrl) || this.baseUrl;

    let headers = {};

    if (requestOptions && requestOptions.headers) {
      headers = { ...headers, ...requestOptions.headers };
    }

    if (this.headers) {
      headers = { ...headers, ...this.headers };
    }

    const promise = callApi<Return>(
      this.getCallUrl(baseUrl, path, params),
      action,
      body,
      headers
    );

    return timeOut(timeout, promise);
  };

  private getCallUrl = (
    baseUrl: string,
    path: string,
    params?: IRequestParameter[]
  ): string => {
    const parametersValue = generateParametersValue(params);
    const url = `${baseUrl}${path}${parametersValue}`;

    return url;
  };

  // #endregion
}

// #endregion

// #region -------------- Helper functions -------------------------------------------------------------------

function generateParametersValue(parameters: IRequestParameter[]) {
  let value = "";

  if (parameters && parameters.length > 0 && parameters[0]) {
    value += `?${parameters[0].key}=${parameters[0].value}`;

    for (let i = 1; i < parameters.length; i += 1) {
      value += `&${parameters[i].key}=${parameters[i].value}`;
    }
  }

  return value;
}

function callApi<T>(
  url: string,
  method = "GET",
  body = {},
  headers?: IRequestHeaders
): Promise<T> {
  const fetchParams: RequestInit = {
    cache: "no-cache",
    headers: {
      "content-type": "application/json",
      "X-Requested-With": "XMLHttpRequest",
      ...headers,
    },
    method,
    mode: "cors",
    redirect: "follow",
    referrer: "no-referrer",
    body:
      method !== "GET" &&
      method !== "DELETE" &&
      body &&
      Object.keys(body).length
        ? JSON.stringify(body)
        : null,
  };

  return fetch(url, fetchParams)
    .then((response) => {
      const contentType = response.headers.get("content-type");
      const isJSON = !!(
        contentType && contentType.indexOf("application/json") !== -1
      );

      // All is ok?
      if (response.ok) {
        if (response.status === 204 || response.status === 205) {
          return Promise.resolve(null);
        }

        // JSON OK response
        if (isJSON) {
          return response.json();
        }

        return response.text();
      }

      // Error occured
      if (isJSON) {
        return response.json().then((error: IApiError) => {
          throw new PeekdataError(error.message, error.errorCode, response);
        });
      }

      return response.text().then((error) => {
        throw new PeekdataError(error, null, response);
      });
    })
    .catch((error) => {
      throw error;
    });
}

function timeOut<T>(milliseconds: number, promise: Promise<T>): Promise<T> {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      reject(new Error("Request timed out"));
    }, milliseconds);

    promise.then(resolve, reject);
  });
}

// #endregion
