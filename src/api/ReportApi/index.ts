import {
  IReportListItemRequest,
  IReportListItemResponse,
} from "../../models/report";
import { IRequestOptions, IRequestParameter } from "../../models/request";
import { BaseApi } from "../base";

export class ReportApi extends BaseApi {
  public getReports = (userId?: string, requestOptions?: IRequestOptions) => {
    const params: IRequestParameter[] = [];
    if (userId) {
      params.push({ key: "userId", value: userId });
    }

    return this.get<IReportListItemResponse[]>({
      path: "/reports/getlist",
      requestOptions,
      params,
    });
  };

  public getReport = (reportId: string, requestOptions?: IRequestOptions) => {
    const params: IRequestParameter[] = [];
    if (reportId) {
      params.push({ key: "reportId", value: reportId });
    }

    return this.get<IReportListItemResponse>({
      path: "/reports/get",
      params: params,
      requestOptions,
    });
  };

  public saveReport = (
    report: IReportListItemRequest,
    requestOptions?: IRequestOptions
  ) => {
    return this.post<IReportListItemResponse, IReportListItemRequest>({
      path: "/reports/add",
      body: report,
      requestOptions,
    });
  };

  public updateReport = (
    reportId: string,
    report: IReportListItemRequest,
    requestOptions?: IRequestOptions
  ) => {
    const params: IRequestParameter[] = [{ key: "reportId", value: reportId }];
    return this.put<IReportListItemResponse, IReportListItemRequest>({
      path: "/reports/update",
      body: report,
      requestOptions,
      params,
    });
  };

  public deleteReport = (
    reportId: string,
    requestOptions?: IRequestOptions
  ) => {
    const params: IRequestParameter[] = [{ key: "reportId", value: reportId }];

    return this.delete<void>({
      path: "/reports/delete",
      params,
      requestOptions,
    });
  };
}
