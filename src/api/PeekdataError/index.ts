import { ApiErrorCode } from '../../models/error';

// #region -------------- Error component -------------------------------------------------------------------

export class PeekdataError extends Error {
  public response: Response;
  public code: ApiErrorCode;

  public constructor(message: string, code: ApiErrorCode, response?: Response) {
    super(message);

    this.code = code;
    this.response = response;
  }

  public toString = (): string => {
    if (!this.code && !this.message) {
      return 'Oops, something went wrong. Have another go.';
    }

    if (!this.code) {
      return this.message;
    }

    if (!this.message) {
      return `Error code: ${this.code}`;
    }

    return `Error code: ${this.code}. ${this.message}`;
  }
}

// #endregion
