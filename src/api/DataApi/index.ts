import {
  INotOptimizedReportResponse,
  IOptimizedReportResponse,
  IReportRequest,
} from "../../models/report";
import { IRequestOptions } from "../../models/request";
import { BaseApi } from "../base";

export class DataApi extends BaseApi {
  /**
   * Gets representation of query to be sent to your database engine
   */
  public getQuery = (
    request: IReportRequest,
    requestOptions?: IRequestOptions
  ) => {
    return this.post<string, IReportRequest>({
      path: "/query/get",
      body: request,
      requestOptions,
    });
  };

  /**
   * Gets the data in JSON format
   */
  public getData = (
    request: IReportRequest,
    requestOptions?: IRequestOptions
  ) => {
    return this.post<INotOptimizedReportResponse, IReportRequest>({
      path: "/data/get",
      body: request,
      requestOptions,
    });
  };

  /**
   * Gets the data in optimized JSON format
   */
  public getDataOptimized = (
    request: IReportRequest,
    requestOptions?: IRequestOptions
  ) => {
    return this.post<IOptimizedReportResponse, IReportRequest>({
      path: "/data/getoptimized",
      body: request,
      requestOptions,
    });
  };

  /**
   * Gets data in CSV format
   */
  public getCSV = (
    request: IReportRequest,
    requestOptions?: IRequestOptions
  ) => {
    return this.post<string, IReportRequest>({
      path: "/file/get",
      body: request,
      requestOptions,
    });
  };

  /**
   * Gets data in Excel format
   */
  public getExcel = (
    request: IReportRequest,
    requestOptions?: IRequestOptions
  ) => {
    return this.post<string, IReportRequest>({
      path: "/file/getexcel",
      body: request,
      requestOptions,
    });
  };
}
