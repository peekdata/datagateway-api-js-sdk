import { IConnectionsHealth, IDataModelsHealth, IOverallHealth } from '../../models/health';
import { IRequestOptions } from '../../models/request';
import { BaseApi } from '../base';

export class HealthcheckApi extends BaseApi {

  /**
   * Gets Engine system's status.
   * If system response is positive system returns message "Service is OK" which means system is ready to serve requests.
   * Otherwise error response is returned.
   */
  public getHealth = (requestOptions?: IRequestOptions) => {
    return this.get<string>({
      path: '/healthcheck',
      requestOptions,
    });
  }

  /**
   * Gets overall system's status with the details
   */
  public getHealthDetails = (requestOptions?: IRequestOptions) => {
    return this.get<IOverallHealth>({
      path: '/healthcheck/overall',
      requestOptions,
    });
  }

  /**
   * Gets database connections health
   */
  public getConnectionsHealth = (requestOptions?: IRequestOptions) => {
    return this.get<IConnectionsHealth>({
      path: '/healthcheck/connections',
      requestOptions,
    });
  }

  /**
   * Gets Data Model's status/health
   */
  public getDataModelsHealth = (requestOptions?: IRequestOptions) => {
    return this.get<IDataModelsHealth>({
      path: '/healthcheck/datamodels',
      requestOptions,
    });
  }
}
