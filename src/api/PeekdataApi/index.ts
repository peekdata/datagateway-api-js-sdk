import { IRequestOptions } from '../../models/request';
import { DataApi } from '../DataApi';
import { DataModelApi } from '../DataModelApi';
import { HealthcheckApi } from '../HealthcheckApi';

export class PeekdataApi {
  public data: DataApi;
  public datamodel: DataModelApi;
  public health: HealthcheckApi;

  constructor(options: IRequestOptions) {
    this.data = new DataApi(options);
    this.datamodel = new DataModelApi(options);
    this.health = new HealthcheckApi(options);
  }
}
